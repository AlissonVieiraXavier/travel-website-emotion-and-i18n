import { DestinationsSection } from "../../components/DestinationsSection/DestinationsSection";
import { EasyStepsSection } from "../../components/EasyStepsSection/EasyStepsSection";
import { FirstSection } from "../../components/FirstSection/FirstSection";
import { Navbar } from "../../components/Navbar/Navbar";
import { PartnersSection } from "../../components/PartnersSection/PartnersSection";
import { ServiceSection } from "../../components/ServiceSection/ServiceSection";
import {SectionVerticalCarousel} from "../../components/SectionVerticalCarousel/SectionVerticalCarousel";
import { Footer } from "../../components/Footer/Footer";


export default function Home() {
    return(<>
      <Navbar/>
      <FirstSection/>
      <ServiceSection/>
      <DestinationsSection/>
      <EasyStepsSection/>
      <SectionVerticalCarousel/>
      <PartnersSection/>
      <Footer/>
    </>)
};

