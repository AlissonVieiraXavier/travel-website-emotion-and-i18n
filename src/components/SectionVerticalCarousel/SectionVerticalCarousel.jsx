import React, { useState } from "react";
//import "./VerticalCarousel.css"; // Importe um arquivo CSS para estilizar o carrossel (veja o exemplo abaixo)
import styled from "@emotion/styled";
import manPerfil from '../../assets/manperfil.png'
import womanPerfil from '../../assets/womanperfil.jpg'
import womanPerfil2 from '../../assets/womanperfil2.jpg'

import { FaAngleDown } from "react-icons/fa6";
import { FaAngleUp } from "react-icons/fa6";
import { useTranslation } from "react-i18next";


const VerticalCarouselContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  z-index:-10;
  justify-content:space-around;
  margin-top: 100px;
  max-width: 100%;
  @media screen and (min-width: 963px) {
    flex-direction: row;
  }
`;
const TextDivContainer = styled.div`
  display: flex;
  flex-direction: column;
  z-index:-10;
  max-width: 100%;
`;
const TextDivSubTitle = styled.h5`
color: var(--TEXT-CLR, #5E6282);
font-family: Poppins;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: normal;
text-transform: uppercase;
max-width: 100%;
`;
const TextDivTitle = styled.h2`
    color: var(--3rd, #14183E);
    font-family: Volkhov;
    font-size: 50px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: capitalize;
    width:409px;
    height: 130px;
    position: relative;
    top: -60px;
    max-width: 100%;
`;
const TextDivOptions = styled.div`
display:flex;
width: 87px;
height: 12px;
flex-shrink: 0;
justify-content: space-around;
`;
const TextDivOption = styled.div`
width: 10px;
height: 9px;
flex-shrink: 0;
border-radius: 100%;
background-color: ${props => props.theme.colors.secondary.gray};
`;
const TextDivOptionActive = styled.div`
width: 10px;
height: 9px;
flex-shrink: 0;
border-radius: 100%;
background-color: ${props => props.theme.colors.primary.blue};
`;
//ContentDiv
const ContentDiv = styled.div`
display: flex;
flex-direction: column;
max-width: 100%;

@media screen and (min-width: 1200px) {
    position: relative;
    top:50px;
    right: 100px;
}

`;
const ContentDivPicture = styled.div`
    margin-top: 25px;
    z-index: -10;
    img{
        width: 68px;
        height: 68px;
        flex-shrink: 0;
        border-radius: 100%;
    }
    @media screen and (min-width: 768px) {
      margin-top: 0px;
    }
`;
const ContentDivComents = styled.div`
z-index:-10;

`;
const ContentDivActualComent = styled.div`
  display: flex;
  flex-direction: column;
  width: 90%;
  height: 20rem;
  flex-shrink: 0;
  border-radius: 10px;
  background: var(--white, #fff);
  box-shadow: 0px 1.852px 3.148px 0px rgba(0, 0, 0, 0),
    0px 8.148px 6.519px 0px rgba(0, 0, 0, 0.01),
    0px 20px 13px 0px rgba(0, 0, 0, 0.01),
    0px 38.519px 25.481px 0px rgba(0, 0, 0, 0.01),
    0px 64.815px 46.852px 0px rgba(0, 0, 0, 0.02),
    0px 100px 80px 0px rgba(0, 0, 0, 0.02);
  @media screen and (min-width: 768px) {
    width: 504px;
    height: 280px;
  }
  
`;
const ContentDivActualComenttext = styled.div`
    color: var(--TEXT-CLR, #5E6282);
    font-family: Poppins;
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: 32px; /* 200% */
    width: 80%;
    padding: 20px;
    @media screen and (min-width: 768px) {
      width: 402px;
      padding: 30px;
    }
`;
const ContentDivComentName = styled.div`
color: var(--TEXT-CLR, #5E6282);
font-family: Poppins;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: normal;
position: relative;
top:10px;
left: 30px;
`;
const ContentDivComentLocation = styled.div`
color: var(--TEXT-CLR, #5E6282);
font-family: Poppins;
font-size: 14px;
font-style: normal;
font-weight: 500;
line-height: normal;
position: relative;
top:15px;
left: 30px;
`;
const ContentDivNextComent = styled.div`
border: 2px solid rgba(0, 0, 0, 0);
opacity: 0.8;
height: 80px;
padding-top: 20px;
border-radius:10px;
box-shadow: 0px 1.852px 3.148px 0px rgba(0, 0, 0, 0),
    0px 8.148px 6.519px 0px rgba(0, 0, 0, 0.01),
    0px 20px 13px 0px rgba(0, 0, 0, 0.01),
    0px 38.519px 25.481px 0px rgba(0, 0, 0, 0.01),
    0px 64.815px 46.852px 0px rgba(0, 0, 0, 0.02),
    0px 100px 80px 0px rgba(0, 0, 0, 0.02);
position: relative;
bottom:25px;
left: 50px;
z-index: -10;
`;
const ContentDivButtons = styled.div`
  display: flex;
  flex-direction: column;
  //border: 2px solid red;
  width: 24px;
  height: 71.5px;
  flex-shrink: 0;
  position: relative;
  left:100%;
  top: -480px;
  button:nth-of-type(1){
    position: relative;
    left: -100px
  }
  button:nth-of-type(2){
    position: relative;
    top: 20px;
    left: -100px
  }
  @media screen and (min-width: 768px) {
    left:550px;
    top: -250px;
    button:nth-of-type(1){
    position: relative;
    left: -40px
    }
    button:nth-of-type(2){
      position: relative;
      top: 20px;
      left: -40px
    }
  }
`;
const ContentDivButton = styled.button`
    width: 100px;
    height: 30px;
    border: none;
    background: transparent;
    font-size: 20px;
`;

export const SectionVerticalCarousel = () => {

    const [elementShowing,setElementShowing] = useState(0);
    const {t} = useTranslation();

    const Coments = [
        {
            
            picture: manPerfil,
            comment: t('manPerfilcomment'),
            name:t('womanPerfilName'),
            location: t('manPerfillocation'),
            previous:2,
            next: 1
        },
        {
            picture: womanPerfil,
            comment: t('womanPerfilcomment'),
            name:t('womanPerfilName'),
            location: t('womanPerfillocation'),
            previous:0,
            next: 2
        },
        {
            picture: womanPerfil2,
            comment: t('womanPerfil2comment'),
            name:t('womanPerfil2Name'),
            location: t('womanPerfil2location'),
            previous:1,
            next: 0
        }
    ];
  return (
    <VerticalCarouselContainer>
      <TextDivContainer>
        <TextDivSubTitle>{t('Testimonials')}</TextDivSubTitle>
        <TextDivTitle>{t('TextDivTitleVerticalCarouselContainer')}</TextDivTitle>
        <TextDivOptions>
          {elementShowing === 0 ? (
            <TextDivOptionActive></TextDivOptionActive>
          ) : (
            <TextDivOption></TextDivOption>
          )}
          {elementShowing === 1 ? (
            <TextDivOptionActive></TextDivOptionActive>
          ) : (
            <TextDivOption></TextDivOption>
          )}
          {elementShowing === 2 ? (
            <TextDivOptionActive></TextDivOptionActive>
          ) : (
            <TextDivOption></TextDivOption>
          )}
        </TextDivOptions>
      </TextDivContainer>
      <ContentDiv>
        <ContentDivPicture>
          <img
            src={Coments[elementShowing].picture}
            alt={Coments[elementShowing].picture}
          />
        </ContentDivPicture>
        <ContentDivComents>
            <ContentDivActualComent>
              <ContentDivActualComenttext>
                {Coments[elementShowing].comment}
              </ContentDivActualComenttext>
              <ContentDivComentName>
                {Coments[elementShowing].name}
              </ContentDivComentName>
              <ContentDivComentLocation>
                {Coments[elementShowing].location}
              </ContentDivComentLocation>
            </ContentDivActualComent>
            <ContentDivNextComent>
              {Coments[elementShowing + 1] ? (
                <>
                  <ContentDivComentName>
                    {Coments[elementShowing + 1].name}
                  </ContentDivComentName>
                  <ContentDivComentLocation>
                    {Coments[elementShowing + 1].location}
                  </ContentDivComentLocation>
                </>
              ):(
                <>
                  <ContentDivComentName>
                    {Coments[0].name}
                  </ContentDivComentName>
                  <ContentDivComentLocation>
                    {Coments[0].location}
                  </ContentDivComentLocation>
                </>)}
            </ContentDivNextComent>
        </ContentDivComents>
        <ContentDivButtons>
          <ContentDivButton
            onClick={() => setElementShowing(Coments[elementShowing].previous)}
          >
            <div>
              <FaAngleUp />
            </div>
          </ContentDivButton>
          <ContentDivButton
            onClick={() => setElementShowing(Coments[elementShowing].next)}
          >
            <div>
              <FaAngleDown />
            </div>
          </ContentDivButton>
        </ContentDivButtons>
      </ContentDiv>
    </VerticalCarouselContainer>
  );
};