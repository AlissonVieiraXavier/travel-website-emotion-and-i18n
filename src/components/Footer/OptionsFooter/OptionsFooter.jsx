import styled from "@emotion/styled"
import { useTranslation } from "react-i18next";

const Container = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: -30px;
    align-items: left;
    @media screen and (min-width: 768px) {
    flex-direction: row;
    margin-left: 30px;
  }
`
const DivLinks = styled.div`
    display: flex;
    flex-direction: column;
    font-family: ${(props) => props.theme.fontes.b};
    margin-left: 50px;
    h5{
        font-size: ${(props) => props.theme.fontes.sizes.h5};
        font-weight: bold;
    }

`
const Options = styled.div`
        display: flex;
        flex-direction: column;
`
const StyledLink = styled.a`
 text-decoration: none;
 color: ${(props) => props.theme.colors.gray};
 opacity: 0.5;
 &.active {
    color: ${(props) => props.theme.colors.gray};
 }
 &.hover{
    color: ${(props) => props.theme.colors.gray};
 }
`;


export const OptionsFooter = () => {

    const {t} = useTranslation();
    return(
        <Container>
            <DivLinks>
                <h5>{t('Company')}</h5>
                <Options>
                    <StyledLink to="/#">{t('About')}</StyledLink>
                    <StyledLink to="/#">{t('Careers')}</StyledLink>
                    <StyledLink to="/#">{t('Mobile')}</StyledLink>
                </Options>
            </DivLinks>
            <DivLinks>
                <h5>{t('Contact')}</h5>
                <Options>
                    <StyledLink to="/#">{t('Help/FAQ')}</StyledLink>
                    <StyledLink to="/#">{t('Press')}</StyledLink>
                    <StyledLink to="/#">{t('Affilates')}</StyledLink>
                </Options>
            </DivLinks>
            <DivLinks>
                <h5>{t('More')}</h5>
                <Options>
                    <StyledLink to="/#">{t('Airlinefees')}</StyledLink>
                    <StyledLink to="/#">{t('Airline')}</StyledLink>
                    <StyledLink to="/#">{t('Lowfaretips')}</StyledLink>
                </Options>
            </DivLinks>
        </Container>
    )
} 