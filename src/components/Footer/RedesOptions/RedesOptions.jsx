import styled from "@emotion/styled"
import { FaFacebook } from "react-icons/fa6";
import { FaInstagram } from "react-icons/fa6";
import { FaYoutube } from "react-icons/fa";
import { useTranslation } from "react-i18next";

const Container = styled.div`
    display: flex;
    flex-direction: column;
    margin: 30px 0px;
`
const Rdes = styled.div`
    display: flex;
    flex-direction: row;
    font-size: ${(props) => props.theme.fontes.sizes.h3};
    svg{
        margin-left: 10px;
    }
    @media screen and (min-width: 768px) {
    flex-direction: row;
    margin-left: 30px;
    justify-content: space-evenly;
  }
`
const TextBottom = styled.h6`
font-family: ${(props) => props.theme.fontes.b};
    font-size: ${(props) => props.theme.fontes.sizes.h8};
    font-weight:${(props) => props.theme.fontes.weights.d} ;
    margin-left: 10px;
`

export const RedesOptions = () =>{

    const {t} = useTranslation();

    return(
        <Container>
            <Rdes>
                <FaFacebook />
                <FaInstagram/>
                <FaYoutube/>
            </Rdes>
            <TextBottom>
                {t('Visitenossasredessociais')}
            </TextBottom>
        </Container>
    )
}