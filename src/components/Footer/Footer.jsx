import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";
import { OptionsFooter } from "./OptionsFooter/OptionsFooter";
import { RedesOptions } from "./RedesOptions/RedesOptions";

const Container = styled.section`
  display: flex;
  flex-direction: column;
  background-color: ${props => props.theme.colors.secondary.blue};
  color: ${props => props.theme.colors.secondary.background};
  
`;

const Content = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: space-around;

  @media screen and (min-width: 768px) {
    flex-direction: row;
  }
`;
const DivTexts = styled.div`
  display: flex;
  flex-direction: column;
  font-family: ${(props) => props.theme.fontes.b};
  padding: 20px;
  margin-bottom: 20px;
  div{
    font-size: ${(props) => props.theme.fontes.sizes.h3};
  }
  div:nth-child(2){
    font-size: ${(props) => props.theme.fontes.sizes.h7};
    padding-right: 0px;
    width: 170px;
    margin-top: 10px;
  }
`;

const BottomRigths = styled.div`
  text-align: center;
  width: 100%;
  margin-bottom: 10px;
  font-family: ${(props) => props.theme.fontes.b};
  font-size: ${(props) => props.theme.fontes.sizes.h8};
`;

export const Footer = () => {
  const { t } = useTranslation();
  return (
    <Container>
      <Content>
        <DivTexts>
          <div>Jaboo.</div>
          <div>{t('textDescriptionFootter')}</div>

        </DivTexts>
        <OptionsFooter/>
          <RedesOptions/>
      </Content>
      <BottomRigths>{t('Allrights')} jaddoagency@jadoo.com</BottomRigths>
    </Container>
  );
};
