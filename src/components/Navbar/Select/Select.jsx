import { useState } from "react";
import styled from "@emotion/styled";
import { FaAngleDown } from "react-icons/fa6";
import { AiOutlineToTop } from "react-icons/ai";
import i18n from "../../../lang/i18n";

const SelectContainer = styled.label`
  display: block;
  width: 100%;
  box-sizing: border-box;
  font-weight: 400;
  font-size: 20px;
  line-height: 24px;
  right: 0px;
  position: absolute;
  left: 35px;
  padding: 20px 0px;
  cursor: pointer;
  
  z-index: 500; /* Defina um valor alto conforme necessário */

  @media screen and (min-width: 768px) {
    left: 88%;
    top: 50px;
    padding: 0px;
  }
  @media screen and (min-width: 1200px) {
    left: 85%;
    top: 50px;
    padding: 0px;
  }
`;

const SelectButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${(props) => props.theme.colors.primary.black};
  font-size: ${(props) => props.theme.fontes.sizes.h6};
  font-weight: ${(props) => props.theme.fontes.weights.c};
  border: none;
  background-color: ${(props) => props.theme.colors.background};
  padding: 20px;
  div:nth-of-type(2) {
    position: relative;
    top: 1.5px;
  }
  @media screen and (min-width: 768px) {
    background-color: transparent;
    padding: 0px;
  }
`;

const UlStyled = styled.ul`
  list-style-type: none;
  position: relative;
  max-width: 250px;
  padding: 20px;
  border-radius: ${(props) => props.theme.borders.radius.c};
  box-shadow: 10px 10px 30px #bebebe, -10px -10px 30px #ffffff;
  color: ${(props) => props.theme.colors.primary.black};
  font-size: ${(props) => props.theme.fontes.sizes.h6};
  font-weight: ${(props) => props.theme.fontes.weights.c};
  background-color: ${(props) => props.theme.colors.background};
  z-index: 5;
  li {
    height: 70px;
  }

  li:hover {
    font-weight: ${(props) => props.theme.fontes.weights.a};
  }

  @media screen and (min-width: 768px) {
    top: 15px;
    left: -10px;
    border: 0.5px solid transparent;
    background-color: transparent;
    max-width: 25px;
    li {
    height: auto;
    }
    li:hover {
      font-weight: ${(props) => props.theme.fontes.weights.a};
    }
  }
`;


export default function Select({ options }) {
  const [actualLang, setActualLang] = useState("EN");

  const changeLang = (lang) => {
    i18n.changeLanguage(lang);
    //altera a string para maiusculo por conta da label
    let langLabel = lang.toUpperCase();
    setActualLang(langLabel);
  };

  const [isOpen, changeVisible] = useState(false);
 
  // Convertendo o objeto em um array
  const optionsArray = Object.values(options);

  return (
    <SelectContainer
      onClick={() => (isOpen ? changeVisible(!isOpen) : changeVisible(true))}
    >
      <SelectButton>
        <div>{actualLang}</div>
        <div>{isOpen ? <AiOutlineToTop /> : <FaAngleDown />}</div>
      </SelectButton>
      {isOpen && (
        <UlStyled>
          {optionsArray.map((opcao) => (
            <li key={opcao.id} onClick={() => changeLang(opcao.value)}>
              {opcao.text}
            </li>
          ))}
        </UlStyled>
      )}
    </SelectContainer>
  );
}
