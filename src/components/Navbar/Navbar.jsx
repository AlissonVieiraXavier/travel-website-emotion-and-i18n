import styled from "@emotion/styled";
import Logo from "../../assets/Logo.png";
import Hamburger from "hamburger-react";

import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import Option from "./Option/Option";
import Select from "./Select/Select";
import Decore from "../Decore/Decore";

const NavContainer = styled.nav`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 3px 15px;
  height: 100%;
  justify-content: space-between;
  z-index: 1;
  overflow: hidden;


  @media screen and (min-width: 768px) {
    & {
      margin-top: 20px;
      flex-direction: row;
      align-items: center;
    }
  }
`;
const ColunaLogo = styled.div`
  display: flex;
  padding-left: 5px;
  align-items: center;
  @media screen and (min-width: 768px) {
    & {
      padding-left: 10%;
    }
  }
`;

const ColunaOption = styled.div`
  display: flex;
  padding: 15px;
  flex-direction: column;
  flex-wrap: wrap;
  width: 80%;
  animation: fadeInLeft 0.5s ease-in-out;
  z-index: 2;
  @media screen and (min-width: 768px) {
    & {
      align-items: center;
      justify-content: right;
      width: 65%;
      flex-direction: row;
      margin-right: 15%;
    }
  }
  @keyframes fadeInLeft {
    from {
      opacity: 0;
      transform: translateX(-20px);
    }
    to {
      opacity: 1;
      transform: translateX(0);
    }
  }
`;
const DivLogo = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  flex-direction: row-reverse;
  align-items: center;
  img {
    max-width: 114.912px;
    height: 25px;
    flex-shrink: 0;
    z-index: 2;
  }
  @media screen and (min-width: 768px) {
    img {
    max-width: 114.912px;
    height: 33.994px;
    flex-shrink: 0;
  }
    div {
      display: none;
    }
  }
`;
const DivOption = styled.div`
`;

export const Navbar = () => {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(false);
  const leanguages = {
    a : {
        id: 1,
        text: "EN",
        value: "en",
    },
    b : {
        id: 2,
        text: "PT",
        value: "pt",
    },
    c : {
        id: 3,
        text: "ES",
        value: "es",
    },
  }

  useEffect(() => {
    const handleResize = () => {
      window.innerWidth > 765 ? setOpen(true) : setOpen(false);
    };
    // Configura um ouvinte de evento para redimensionamento da janela
    window.addEventListener("resize", handleResize);
    // Chama handleResize imediatamente para definir o estado inicial
    handleResize();
    // Remove o ouvinte de evento ao desmontar o componente
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []); // O segundo argumento vazio [] garante que o efeito só é executado uma vez no montante e desmontante do componente

  return (
    
    <NavContainer>
      <Decore/>
      <ColunaLogo>
        <DivLogo>
          <img src={Logo} alt="logotipo Jaboo" />
          <div>
            <Hamburger toggled={isOpen} toggle={setOpen} size={22} />
          </div>
        </DivLogo>
      </ColunaLogo>
      {isOpen ? (
        <ColunaOption>
          <DivOption><Option path="/Desitnations">{t('Desitnations')}</Option></DivOption>
          <DivOption><Option path="/Hotels">{t('Hotels')}</Option></DivOption>
          <DivOption><Option path="/Flights">{t('Flights')}</Option></DivOption>
          <DivOption><Option path="/Bookings">{t('Bookings')}</Option></DivOption>
          <DivOption><Option path="/Login">{t('Login')}</Option></DivOption>
          <DivOption><Option path="/Sign-up" border="solid">{t('Sign-up')}</Option></DivOption>
          <DivOption><Select options={leanguages}/></DivOption>
        </ColunaOption>
      ) : (
        <></>
      )}
       
    </NavContainer>
  );
};