import styled from "@emotion/styled";

import imgLondon from "../../../assets/london.png";
import imgRoma from "../../../assets/rome.png";
import imgFulleurope from "../../../assets/fulleurope.png";
import { useTranslation } from "react-i18next";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  z-index: -1;
  //border: 2px solid red;
  padding-right: 50px;
  justify-content: center;
  align-items: center;
  @media screen and (min-width: 765px) {
    padding-right: 50px;
  }
`;
const Card1 = styled.div`
  width: 314px;
  height: 407px;
  flex-shrink: 0;
  border-radius: ${props => props.theme.borders.a};
  //border: 2px solid blue;
  padding: 20px;
  position: relative;
  left: 20px;
  img {
    max-width: 420px;
    position: relative;
    left: -50px;
  }
  @media screen and (min-width: 765px) {
    position: relative;
    left: 70px;
    width: 314px;
    height: 457px;
    flex-shrink: 0;
    border-radius: ${props => props.theme.borders.a};
    //border: 2px solid blue;
    padding: 20px;
    img {
    max-width: none;
    position: relative;
    left: -80px;
  }
  }
`;
const DivText = styled.div`
  position: relative;
  bottom: 300px;
  width: 314px;
  height: 130px;
  flex-shrink: 0;
  border-radius: 0px 0px 24px 24px;
  background: ${props => props.theme.colors.primary.branco};
  padding-top: 20px;
  div {
    color: var(--TEXT-CLR, ${props => props.theme.colors.primary.gray});
    font-family: ${props => props.theme.fontes.b};
    font-size: ${props => props.theme.fontes.sizes.h5};
    font-style: normal;
    font-weight:${props => props.theme.fontes.weights.c};
    line-height: 124.5%; /* 22.41px */
  }
`;
const NameAndPlace = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-around;
`;
const Days = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
  position: relative;
  top:70px;
  left:30px;
  i{
    margin-left: 5px;
  }
`;
const DecoreSvg = styled.div`
  position: relative;
  left: 5px;
  bottom: -60px;
  z-index: -10;
  i {
    display: none;
  }
  @media screen and (min-width: 765px) {
    i {
      display: block;
    }
  }
  @media screen and (min-width: 1128px) {
    i {
      display: none;
    }
  }
  @media screen and (min-width: 1226px) {
    i {
      display: block;
    }
  }
`;

export const CardsDestinationDiv = () => {

  const iconCard = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
    >
      <path
        d="M18.0682 2.76033L11.1932 17.7603C11.1357 17.886 11.0381 17.989 10.9158 18.0533C10.7935 18.1177 10.6533 18.1396 10.5172 18.1158C10.3811 18.092 10.2567 18.0237 10.1635 17.9217C10.0703 17.8197 10.0135 17.6896 10.0021 17.5519L9.42083 10.579L2.44789 9.99771C2.31017 9.98627 2.18013 9.92951 2.0781 9.8363C1.97607 9.7431 1.90781 9.61871 1.88399 9.48258C1.86017 9.34646 1.88213 9.20628 1.94645 9.08397C2.01077 8.96166 2.11381 8.86411 2.23945 8.80658L17.2395 1.93158C17.3554 1.87844 17.4849 1.86205 17.6104 1.88461C17.736 1.90718 17.8516 1.96762 17.9418 2.05781C18.032 2.14801 18.0924 2.26365 18.115 2.38919C18.1375 2.51474 18.1212 2.64418 18.068 2.76014L18.0682 2.76033Z"
        fill="#080809"
      />
    </svg>
  );
  const svgDecore = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="98"
      height="254"
      viewBox="0 0 98 254"
      fill="none"
    >
      <path
        d="M8.95816 14.2344C15.985 5.71795 33.7527 -7.37604 48.6094 8.37933C63.4661 24.1347 43.423 53.6228 31.5443 66.3974C25.6886 69.9459 13.4754 75.9784 11.4677 71.7202C8.95816 66.3974 7.45241 55.2196 28.0309 45.6387C41.248 39.961 69.7902 33.9286 78.2224 55.2196C82.0704 64.4457 82.7396 85.8787 54.6324 97.8017L19.4984 113.238M19.4984 113.238C6.11398 113.774 -13.1261 107.392 16.9888 77.5752C26.5252 72.9621 49.6133 63.6296 65.6746 63.2038C74.8763 65.1554 92.5772 74.8074 89.7665 97.8017C90.1011 102.415 83.8439 113.876 56.1382 122.819C49.7806 124.238 35.5597 127.183 29.5367 127.609L7.95439 130.803L19.4984 113.238ZM19.4984 113.238C28.2808 110.754 49.0085 106.531 61.6592 109.512C77.4727 113.238 83.7435 110.044 96.2913 130.803C98.4663 141.448 96.8936 163.272 73.2033 165.401C49.5129 167.53 34.2212 163.449 29.5367 161.143V141.448M29.5367 141.448C46.7691 143.045 83.7435 141.874 93.7818 124.415C98.6336 111.996 99.0017 86.6239 61.6592 84.4948C52.6248 84.8496 33.3513 85.9852 28.5329 87.6884L29.5367 125.48C47.1037 127.077 84.0446 134.635 91.2722 152.094C95.1202 162.207 96.3917 183.498 70.6937 187.756C61.1573 188.111 39.575 186.798 29.5367 178.708M29.5367 141.448V178.708M29.5367 141.448C40.4115 144.11 64.3696 151.455 73.2033 159.546C83.9108 168.417 99.604 190.418 76.7167 207.45C69.5225 210.112 50.4163 212.241 31.5443 199.466M29.5367 178.708L31.5443 199.466M29.5367 178.708C41.5827 187.224 66.1765 207.876 68.1841 222.354C69.0207 231.225 66.7788 249.713 51.119 252.694C47.1037 253.404 37.1658 253.333 29.5367 247.371L31.5443 199.466"
        stroke="#84829A"
      />
    </svg>
  );

  const {t} = useTranslation();

  return (
    <Container>
      <Card1>
        <img src={imgRoma} alt="s" />
        <DivText>
          <NameAndPlace>
            <div>Rome, Italty</div>
            <div>$5,42k</div>
          </NameAndPlace>
          <Days>
            {iconCard}
            <i>10 {t('DaysTrip')}</i>
          </Days>
        </DivText>
      </Card1>
      <Card1>
        <img src={imgLondon} alt="s" />
        <DivText>
          <NameAndPlace>
            <div>London, UK</div>
            <div>$4.2k</div>
          </NameAndPlace>
          <Days>
            {iconCard}
            <i>12 {t('DaysTrip')}</i>
          </Days>
        </DivText>
      </Card1>
      <Card1>
        <img src={imgFulleurope} alt="s" />
        <DivText>
          <NameAndPlace>
            <div>Full Europe</div>
            <div>$15k</div>
          </NameAndPlace>
          <Days>
            {iconCard}
            <i>28 {t('DaysTrip')}</i>
          </Days>
        </DivText>    
      </Card1>
      <DecoreSvg><i>{svgDecore}</i></DecoreSvg>
    </Container>
  );
};
