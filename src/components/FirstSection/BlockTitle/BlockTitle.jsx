import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";
import svgSpan  from "../../../assets/Span.png";
import { BtnAction } from "../../BtnAction/BtnAction";
import {VideoPlayer} from "../../VideoPlayer/VideoPlayer";

const Container = styled.section`
  z-index: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: left;
  overflow: hidden;
 //0 border:2px solid blue;
  width: 100%;
  margin: 10px;
  height: 600px;
  @media screen and (min-width: 768px) {
    max-width: 60%;
    margin: 50px 0px 10px 80px;
    height: 750px;
  }
  @media screen and (min-width: 1200px) {
    margin: 50px 0px 10px 150px;
  }
`;

const TopTitle = styled.h4`
  z-index: 5;
  font-size: ${(props) => props.theme.fontes.sizes.h4};
  color: ${(props) => props.theme.colors.primary.orange};
  font-style: normal;
  font-family: ${(props) => props.theme.fontes.b};
  font-weight: ${(props) => props.theme.fontes.weights.a};
  line-height: normal;
  text-transform: uppercase;
  max-width: 410px;
  
`;
const MainTitle = styled.h1`
  font-size: ${(props) => props.theme.fontes.sizes.h2};
  color: ${(props) => props.theme.colors.primary.blue};
  font-style: normal;
  position: relative;
  top: -80px;
  font-family: ${(props) => props.theme.fontes.b};
  font-weight: ${(props) => props.theme.fontes.weights.a};
  line-height: 59px;
  width: 100%;
  margin-top: 70px;
  @media screen and (min-width: 768px) {
    font-size: ${(props) => props.theme.fontes.sizes.h1};
    line-height: 89px;
    max-width: 545px;
    max-height: 267px;
  }
`;
const MainTitle2 = styled.span`
  font-size: ${(props) => props.theme.fontes.sizes.h2};
  color: ${(props) => props.theme.colors.primary.blue};
  font-style: normal;
  line-height: 59px;
  font-family: ${(props) => props.theme.fontes.b};
  font-weight: ${(props) => props.theme.fontes.weights.a};
  //border:2px solid blue;
  margin: 0 0;
  background-image: url(${svgSpan}); /* Ajusta o tamanho da imagem para cobrir todo o elemento */
  background-size:310px;
  background-position: bottom left; /* Centraliza a imagem dentro do elemento */
  background-repeat: no-repeat;
  @media screen and (min-width: 768px) {
    font-size: ${(props) => props.theme.fontes.sizes.h1};
    line-height: 89px;
    max-width: 545px;
    max-height: 267px;
  }
`;
const TextDescription = styled.p`
  color: ${(props) => props.theme.colors.primary.gray};
  font-family: ${(props) => props.theme.fontes.b};
  font-size: ${(props) => props.theme.fontes.sizes.h7};
  font-style: normal;
  font-weight: ${(props) => props.theme.fontes.weights.c};
  line-height: 30px;
  max-width: 477px;
  height: 90px;
  position: relative;
  top: -100px;
  @media screen and (min-width: 768px) {
    top: -60px;
  }
`;
const ButtonsDiv = styled.div`
  position: relative;
  top: -50px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  flex-wrap: wrap;
  @media screen and (min-width: 768px) {
    position: relative;
    top: -50px;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
  }
`; 
const VideoPlayerDiv = styled.div`
 margin-left: -30px;
 display: flex;
 flex-direction: row;
 p{
  color: ${(props)=> props.theme.colors.primary.gray};
  font-family: ${(props)=> props.theme.fontes.b};
  font-size: 17px;
  font-style: normal;
  font-weight: 500;
  line-height: normal;
  position:relative;
 }
 @media screen and (min-width: 768px) {
  margin-left: 20px;
  p{
    left: -10px;
  }
 }
`; 



export const BlockTitle = () => {
  const { t } = useTranslation();

  return (
    <Container>
      <div>
        <TopTitle>{t("BlockTitleh4")}</TopTitle>
      </div>
      <div>
      <MainTitle>
          {t("Travel")},
          <MainTitle2>
            {t("enjoy")}
          </MainTitle2><br></br>
          {t("BlockTitleh1")}
        </MainTitle>  
      </div>
      <div>
        <TextDescription>{t('TextDescription')}</TextDescription>
      </div>
      <ButtonsDiv>
          <BtnAction color="">
              {t('FindOutMore')}
          </BtnAction>
          <VideoPlayerDiv>
            <VideoPlayer/>
            <p>{t('PlayDemo')}</p>
          </VideoPlayerDiv>
      </ButtonsDiv>
    </Container>
  );
};

