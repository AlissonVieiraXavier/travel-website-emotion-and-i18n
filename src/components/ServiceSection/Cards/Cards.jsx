import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";
import Card1 from '../../../assets/card1.svg';
import Card2 from '../../../assets/card2.svg';
import Card3 from '../../../assets/card3.png';
import Card4 from '../../../assets/card4.svg';

const ContainerCards = styled.div`
display: flex;
z-index: -10;
flex-direction: column;
align-items: center;
justify-content: center;
  @media screen and (min-width: 765px) {
    flex-direction: row;
  }
`;
const Card = styled.div`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
width: 267px;
height: 314px;
flex-shrink: 0;
padding: 20px;

`;
const CardPersonalizado = styled.div`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
width: 267px;
height: 314px;
flex-shrink: 0;
border-radius: ${props => props.theme.borders.radius.d};
background: var(--white, ${props => props.theme.colors.primary.branco});
box-shadow: 0px 1.852px 3.148px 0px rgba(0, 0, 0, 0.00), 0px 8.148px 6.519px 0px rgba(0, 0, 0, 0.01), 0px 20px 13px 0px rgba(0, 0, 0, 0.01), 0px 38.519px 25.481px 0px rgba(0, 0, 0, 0.01), 0px 64.815px 46.852px 0px rgba(0, 0, 0, 0.02), 0px 100px 80px 0px rgba(0, 0, 0, 0.02);
margin: 20px;
position: relative;

`;
const CardPersonalizadoDivUnder = styled.div`
width: 100px;
height: 100px;
flex-shrink: 0;
border-radius: 30px 0px 10px 0px;
background: ${props => props.theme.colors.primary.orange};
position: relative;
z-index: -100;
bottom: 5px;
left: -115px;
`;
const CardImage = styled.div`
position: relative;

img{
    max-height: 120px;
}
`;
const CardImagePersonalizado = styled.div`
    position: relative;
    top: 30px;
    img{
        max-height: 200px;
    }
`;
const CardImageLocalEvents = styled.div`
    position: relative;
    img{
        max-height: 150px;
    }
`;
const DivLocalEvents = styled.div`
        border-radius: 18px 5px 10px 5px;
        background: ${props => props.theme.colors.secondary.background};
        width: 50px;
        height: 49px;
        position: absolute;
        bottom: 25px;
        left: 20px;
        z-index: -100;
`;


const Cardtitle = styled.h4`
color: var(--text-heading-color, ${props => props.theme.colors.secondary.blue2});
text-align: center;
font-family: ${props => props.theme.fontes.c};
font-size: ${props => props.theme.fontes.sizes.h4};
font-style: normal;
font-weight: ${props => props.theme.fontes.weights.b};
line-height: 10px;
`;
const Cardtext = styled.p`
color: var(--TEXT-CLR, ${props => props.theme.colors.primary.gray});
text-align: center;
font-family: ${props => props.theme.fontes.b};
font-size: ${props => props.theme.fontes.sizes.h7};
font-style: normal;
font-weight: ${props => props.theme.fontes.weights.c};
line-height: 26px;
padding: 0px 10px;

`;

export const Cards = () => {

    const {t} = useTranslation();

    return(
        <ContainerCards>
            <Card>
                <CardImage><img src={Card1} alt="Card calculated Weather"/></CardImage>
                <Cardtitle>{t('CalculatedWeather')}</Cardtitle>
                <Cardtext>{t('Card1Text')}</Cardtext>
            </Card>
            <CardPersonalizado>
                <CardImagePersonalizado><img src={Card2} alt="Card Best Flights" /></CardImagePersonalizado>
                <Cardtitle>{t('BestFlights')}</Cardtitle>
                <Cardtext>{t('Card2Text')}</Cardtext>
                <CardPersonalizadoDivUnder></CardPersonalizadoDivUnder>
            </CardPersonalizado>
            <Card>
                <CardImageLocalEvents><img src={Card3} alt="Card Local Events"/> <DivLocalEvents></DivLocalEvents></CardImageLocalEvents>
                <Cardtitle>{t('LocalEvents')}</Cardtitle>
                <Cardtext>{t('Card3Text')}</Cardtext>
            </Card>
            <Card>
                <CardImage><img src={Card4} alt="Card Customization"/></CardImage>
                <Cardtitle>{t('Customization')}</Cardtitle>
                <Cardtext>{t('Card4Text')}</Cardtext>
            </Card>
        </ContainerCards>
    )
};
